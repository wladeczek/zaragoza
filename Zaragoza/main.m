//
//  main.m
//  Zaragoza
//
//  Created by WLADYSLAW SURALA on 28/09/16.
//  Copyright © 2016 WLADYSLAW SURALA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
