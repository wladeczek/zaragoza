//
//  ViewController.m
//  Zaragoza
//
//  Created by WLADYSLAW SURALA on 28/09/16.
//  Copyright © 2016 WLADYSLAW SURALA. All rights reserved.
//

#import "ViewController.h"
#import "CellBusStop.h"
#import <AFNetworking/AFNetworking.h>

#define kAPI_KEY @"AIzaSyAu7UFnrKCE0paSscJIn_ZygQo-xKD5dtE"
@interface ViewController (){
    
    NSMutableDictionary *downloadsForVisibleRows;
    NSArray *arrayBusStops;
    NSMutableDictionary *dictionaryDownloadsMapImages;
    NSMutableDictionary *dictionaryDownloadsRealTimeData;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    arrayBusStops = @[];
    dictionaryDownloadsMapImages = [NSMutableDictionary new];
    dictionaryDownloadsRealTimeData = [NSMutableDictionary new];
    [self downloadBusStopsData];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)refresh
{
    [self downloadBusStopsData];
}

-(void)downloadBusStopsData
{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL = [NSURL URLWithString:@"http://api.dndzgz.com/services/bus"];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    __block __weak id weakSelf = self;
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error);
            [weakSelf displayConnectionError];
        } else {
            NSDictionary *responseDict = (NSDictionary*)responseObject;
            [weakSelf parseDataBusStops:responseDict];
        }
    }];
    [dataTask resume];
}

-(void)downloadRealTimeDataForIndexPath:(NSIndexPath*)indexPath
{
    NSDictionary *dictionaryBusStop = arrayBusStops[indexPath.row];
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    NSString *stringURL = [NSString stringWithFormat:@"http://api.dndzgz.com/services/bus/%@",dictionaryBusStop[@"id"]];
    NSURL *URL = [NSURL URLWithString:stringURL];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error downloading data for bus stop: %@", error);
        } else {
            CellBusStop *cell = [tableView_ cellForRowAtIndexPath:indexPath];
            NSDictionary *responseDict = (NSDictionary*)responseObject;
            NSArray *estimates = responseDict[@"estimates"];
            for (NSDictionary *dictionaryData in estimates) {
                [cell addRealTimeInfo:dictionaryData];
            }
            [dictionaryDownloadsRealTimeData removeObjectForKey:indexPath];
        }
    }];
    [dictionaryDownloadsRealTimeData setObject:dataTask forKey:indexPath];
    [dataTask resume];
    
}
-(void)stopOngoingDownloads
{
    for (NSURLSessionDownloadTask *task in dictionaryDownloadsMapImages.allValues) {
        [task cancel];
    }
    [dictionaryDownloadsMapImages removeAllObjects];
    for (NSURLSessionDataTask *task in dictionaryDownloadsRealTimeData.allValues) {
        [task cancel];
    }
    [dictionaryDownloadsRealTimeData removeAllObjects];
}
-(void)downloadImagesAndRealTimeDataForVisibleRows
{
    [self stopOngoingDownloads];
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSArray *visibleIndexPaths = [tableView_ indexPathsForVisibleRows];
    for (__block NSIndexPath *indexPath in visibleIndexPaths){
        
        [self downloadRealTimeDataForIndexPath:indexPath];
        __block NSInteger row = indexPath.row;
        NSString *filenameImage = [NSString stringWithFormat:@"%ld.png",(long)row];
        NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
        __block NSURL *filepath = [documentsDirectoryURL URLByAppendingPathComponent:filenameImage];
        
        if([[NSFileManager defaultManager]fileExistsAtPath:[filepath path]]){
            CellBusStop *cell = [tableView_ cellForRowAtIndexPath:indexPath];
            UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:filepath]];
            [cell setBusMapImage:image];
        }
        
        NSDictionary *dictionaryBusStop = arrayBusStops[row];
        NSString *latitude = dictionaryBusStop[@"lat"];
        NSString *longitude = dictionaryBusStop[@"lon"];
        
        NSString *urlString = @"https://maps.googleapis.com/maps/api/staticmap";
        NSString *center = [NSString stringWithFormat:@"%@,%@",latitude,longitude];
        NSString *zoom = @"18";
        NSString *size = @"200x200";
        NSDictionary *parameters = @{@"center": center, @"zoom": zoom,@"size":size,@"key":kAPI_KEY};
        NSError *error = nil;
        NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer]requestWithMethod:@"GET" URLString:urlString parameters:parameters error:&error];
        
        NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
            
            
            return filepath;
        } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        
            CellBusStop *cell = [tableView_ cellForRowAtIndexPath:indexPath];
            UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:filePath]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [cell setBusMapImage:image];
            });
            [dictionaryDownloadsMapImages removeObjectForKey:indexPath];
            
        }];
        [downloadTask resume];
        
        [dictionaryDownloadsMapImages setObject:downloadTask forKey:indexPath];



    }
}

-(void)displayConnectionError
{
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Connection error" message:@"Please refresh aftet some time and check your connectivity." preferredStyle:UIAlertControllerStyleAlert];
    [self presentViewController:controller animated:YES completion:nil];
}

-(void)parseDataBusStops:(NSDictionary*)responseObjectJSON
{
    NSArray *arrayLocations = responseObjectJSON[@"locations"];
    
    if(arrayLocations){
        arrayBusStops = arrayLocations;
    }else{
        [self displayConnectionError];
    }
    [tableView_ reloadData];
    [self downloadImagesAndRealTimeDataForVisibleRows];
}
#pragma mark UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrayBusStops count];
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return @"Bus Stop";
            break;
            
        default:
            return @"";
            break;
    }
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"identifierCell";
    CellBusStop *cell = (CellBusStop*)[tableView dequeueReusableCellWithIdentifier:identifier];
    
    NSInteger row = indexPath.row;
    
    NSDictionary *dictionaryBusStop = arrayBusStops[row];
    [cell configureWithDictionary:dictionaryBusStop];
    
    return cell;
}
#pragma mark UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90.0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30.0;
}
#pragma mark UIScrollViewDelegate

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self downloadImagesAndRealTimeDataForVisibleRows];
}
@end
