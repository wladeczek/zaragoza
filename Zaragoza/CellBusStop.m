//
//  CellBusStop.m
//  Zaragoza
//
//  Created by WLADYSLAW SURALA on 28/09/16.
//  Copyright © 2016 WLADYSLAW SURALA. All rights reserved.
//

#import "CellBusStop.h"


@implementation CellBusStop

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureWithDictionary:(NSDictionary*)dictionaryBusStop
{
    labelName.text = (NSString*)dictionaryBusStop[@"title"];
    labelDescription.text = (NSString*)dictionaryBusStop[@"subtitle"];
    labelID.text = (NSString*)dictionaryBusStop[@"id"];
    [labelID sizeToFit];
    [labelDescription sizeToFit];
    [labelName sizeToFit];
    [imageViewMapThumbnail setImage:nil];
    [indicatorViewLoading startAnimating];

}

-(void)setBusMapImage:(UIImage*)image
{
    [indicatorViewLoading stopAnimating];
    [imageViewMapThumbnail setImage:image];

}
-(void)addRealTimeInfo:(NSDictionary *)info
{
    NSMutableString *infoString = [NSMutableString new];
    [infoString appendString:@"\n"];
    
    [infoString appendString:@"line: "];
    [infoString appendString:info[@"line"]];
    [infoString appendString:@" direction: "];
    [infoString appendString:info[@"direction"]];
    [infoString appendString:@" estimate: "];
    id estimated = info[@"estimate"];
    if([estimated class] == [NSNull class]){
        [infoString appendFormat:@"0"];
    }else{
        long long estimate = [info[@"estimate"] longLongValue];
        [infoString appendFormat:@"%lld",estimate];
    }
    
    [infoString appendString:@"\n"];
    
    labelDescription.text = [labelDescription.text stringByAppendingString:infoString];
    [labelDescription sizeToFit];
}
@end
