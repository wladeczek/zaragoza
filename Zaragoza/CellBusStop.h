//
//  CellBusStop.h
//  Zaragoza
//
//  Created by WLADYSLAW SURALA on 28/09/16.
//  Copyright © 2016 WLADYSLAW SURALA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellBusStop : UITableViewCell{
    
    IBOutlet UIImageView *imageViewMapThumbnail;
    IBOutlet UILabel *labelName;
    IBOutlet UILabel *labelDescription;
    IBOutlet UILabel *labelID;
    IBOutlet UIActivityIndicatorView *indicatorViewLoading;
}

-(void)configureWithDictionary:(NSDictionary*)dictionaryBusStop;
-(void)setBusMapImage:(UIImage*)image;
-(void)addRealTimeInfo:(NSDictionary*)info;
@end
