//
//  ViewController.h
//  Zaragoza
//
//  Created by WLADYSLAW SURALA on 28/09/16.
//  Copyright © 2016 WLADYSLAW SURALA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate>{
    
    IBOutlet UITableView *tableView_;
    
}

-(IBAction)refresh;

@end

